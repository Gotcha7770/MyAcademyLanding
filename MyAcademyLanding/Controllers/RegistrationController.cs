﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyAcademyLanding.Controllers
{
    public class User
    {
        public string ParentName { get; set; }
        public string ChildName { get; set; }
        public int ChildAge { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }

    public class Consumer
    {
        public static readonly int MexRequestsCount = 10;

        public DateTimeOffset FirstRequestTime { get; }
        public int RequestsCount { get; set; }

        public Consumer(DateTimeOffset requestTime)
        {
            FirstRequestTime = requestTime;
            RequestsCount = 1;
        }
    }

    [Route("api/[controller]")]
    public class RegistrationController : Controller
    {
        public static readonly TimeSpan Day = TimeSpan.FromDays(1);

        private readonly string _fromEmail = "landing@academy-center.ru";
        private readonly string _fromPassword = "wJ5sk#04";
        private readonly string _fromServer = "mail.academy-center.ru";
        private readonly int _serverPort = 587;
        private readonly string _fromName = "MyAcademyLanding";
        private readonly string _managerEmail = "info@abcomsk.ru";
        private readonly string _subjectText = "Регистрация";

        [HttpPost("[action]")]
        public ActionResult Register([FromBody] User user)
        {            
            if (user == null)
                return BadRequest();

            IPAddress remoteIp = Request.HttpContext.Connection.RemoteIpAddress;

            try
            {
                SendEmail(user);
            }
            catch (Exception ex)
            {

                return NotFound();
            }

            return Ok();
        }

        private void SendEmail(User user)
        {
            // отправитель - устанавливаем адрес и отображаемое в письме имя
            MailAddress from = new MailAddress(_fromEmail, _fromName);
            // кому отправляем
            MailAddress to = new MailAddress(_managerEmail);
            // создаем объект сообщения
            MailMessage mail = new MailMessage(from, to);
            // тема письма
            mail.Subject = _subjectText;

            // текст письма
            mail.Body = $"Имя родителя: {user.ParentName}\n" +
                $"Имя ребенка: {user.ChildName}\n" +
                $"Возраст ребенка: {user.ChildAge}\n" +
                $"Телефон: {user.PhoneNumber}\n " +
                $"Почта: {user.Email}";

            // адрес smtp-сервера и порт, с которого будем отправлять письмо
            SmtpClient smtp = new SmtpClient(_fromServer, _serverPort);
            // логин и пароль
            smtp.Credentials = new NetworkCredential(_fromEmail, _fromPassword);

            smtp.Send(mail);
        }
    }
}