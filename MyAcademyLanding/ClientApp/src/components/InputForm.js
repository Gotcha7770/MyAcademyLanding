﻿import React, { Component } from 'react';
import ym from 'react-yandex-metrika';

const successMessage = 'Ваша заявка успешно отправлена. ' +
    'В ближайшее время с Вами свяжется наш менеджер, чтобы записать Вас на пробное занятие.';

const errorMessage = 'При отправке заявки произошла какая-то ошибка( '
    + 'Пожалуйста, свяжитесь с менеджером по телефону указанному на сайте'
    + 'Приносим свои извинения за доставленные неудобства!';

export class InputForm extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(element) {
        element.preventDefault();
        const form = element.currentTarget;
        this.submitForm(form);

        ym('reachGoal', 'zapis');

        return true;
    }

    submitForm(form) {
        const response = fetch('api/registration/register', {
            method: 'POST',
            headers: new Headers({
                "Content-Type": "application/json",
                Accept: "application/json"
            }),
            body: JSON.stringify({
                parentName: form.parentName.value,
                childName: form.childName.value,
                childAge: form.childAge.value,
                phoneNumber: form.phoneNumber.value,
                email: form.email.value
            })
        }).then(function (response) {
            //alert(response.headers.get('Content-Type'));
            //alert(response.status);
            if (response.ok) {
                alert(successMessage);
            }
            else {
                alert(errorMessage);
            }

            return;
        }).catch(alert);
    }

    render() {
        return (
            <form className="input-form" onSubmit={e => this.handleSubmit(e)}>
                <input
                    className="text-input"
                    type="text"
                    name="parentName"
                    placeholder="Фамилия и имя родителя"
                    maxLength={64}
                    required />
                <span className="validity"></span>
                <input
                    className="text-input"
                    type="text"
                    name="childName"
                    placeholder="Имя ребёнка"
                    maxLength={64}
                    required />
                <span className="validity"></span>
                <input
                    className="number-input"
                    type="number"
                    name="childAge"
                    placeholder="Возраст ребёнка"
                    min={1}
                    max={17}
                    required />
                <span className="validity"></span>
                <input
                    className="phone-input"
                    type="text"
                    name="phoneNumber"
                    placeholder="Телефон"
                    required />
                <span className="validity"></span>
                <input
                    className="email-input"
                    type="email"
                    name="email"
                    placeholder="E-mail"
                    maxLength={64}
                    required />
                <span className="validity"></span>
                <button className="btn btn-default" type="submit">Записаться на пробное занятие</button>                

            </form>
        );
    }
}