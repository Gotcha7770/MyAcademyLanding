﻿import React, { Component } from 'react';
import * as $ from 'jquery';
import { Link, } from 'react-scroll';
import { slide as Menu } from 'react-burger-menu';
import { ToggleButtonGroup, ToggleButton, Button, Carousel } from 'react-bootstrap';
import { YMInitializer } from 'react-yandex-metrika';
import { InputForm } from './InputForm';

function randomColor() {
    return { background: getRandomColor() };
};

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getOffset() {
    let offset = 0;
    if (window.innerWidth > 1366)
        offset = -100;
    if (window.innerWidth <= 1366 && window.innerWidth > 1024)
        offset = -85;
    if (window.innerWidth <= 1024)
        offset = -65;

    return offset;
}

export class Home extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    updateDimensions() {
        let height = 0;
        if (window.innerWidth > 1136)
            height = window.innerHeight - 100;
        if (window.innerWidth <= 1136 && window.innerWidth > 1024)
            height = window.innerHeight - 85;
        if (window.innerWidth <= 1024) {
            $('.block').not('.promo-block').not('#programms').height('auto');
            return;
        }

        console.log(`updating dimensions, height=${height}`);
        console.log(`inner width = ${window.innerWidth}`);
        console.log(`inner height = ${window.innerHeight}`);
        $('.block').not('.promo-block').not('#programms').height(height);
    }

    handleBlockLoaded() {
        console.log('handle block loaded');
        this.updateDimensions();
    }

    render() {

        return <div id="outer-container" data-spy="scroll" data-target=".header">

            <header className="header">

                <Link className="logo-wrap" to="promo" offset={getOffset()} spy={true} smooth={true} duration={500}>
                    <img className="logo-img logo-img-main" src={require('../img/svg/logo.svg')} />
                </Link>

                <nav className="navbar">
                    <Link className="link nav-item" to="about" offset={getOffset()} smooth={true} duration={500}>О нас</Link>
                    <Link className="link nav-item" to="advantages" offset={getOffset()} smooth={true} duration={500}>Преимущества</Link>
                    <Link className="link nav-item" to="programms" offset={getOffset()} smooth={true} duration={500}>Программы</Link>
                    <Link className="link nav-item" to="starteducation" offset={getOffset()} smooth={true} duration={500}>Начать обучение</Link>
                    <Link className="link nav-item" to="contacts" offset={getOffset()} smooth={true} duration={500}>Контакты</Link>
                </nav>

                <div className="socials">
                    <a className="social-item" target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/akademy_center/"><i className="fab fa-instagram fa-3x" /></a>
                    <a className="social-item" target="_blank" rel="noopener noreferrer" href="https://vk.com/academy_center"><i className="fab fa-vk fa-3x" /></a>
                    <a className="social-item" target="_blank" rel="noopener noreferrer" href="https://ok.ru/profile/561815399930"><i className="fab fa-odnoklassniki fa-3x" /></a>
                </div>

            </header>

            <Menu pageWrapId={"page-wrap"} outerContainerId={"outer-container"}>
                <Link className="link menu-item" to="about" offset={getOffset()} smooth={true} duration={500}>О нас</Link>
                <Link className="link menu-item" to="advantages" offset={getOffset()} smooth={true} duration={500}>Преимущества</Link>
                <Link className="link menu-item" to="programms" offset={getOffset()} smooth={true} duration={500}>Программы</Link>
                <Link className="link menu-item" to="starteducation" offset={getOffset()} smooth={true} duration={500}>Начать обучение</Link>
                <Link className="link menu-item" to="contacts" offset={getOffset()} smooth={true} duration={500}>Контакты</Link>
            </Menu>

            <main id="page-wrap" className="wrapper" onLoad={this.handleBlockLoaded.bind(this)}>

                <div id="promo" className="promo-block block">
                    <Carousel>
                        <Carousel.Item>
                            <img alt="Прекрасное начало" src={require('../img/slider_1.jpg')} />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img alt="На старт. Внимание. Достижения!" src={require('../img/slider_2.jpg')} />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img alt="Дни открытых дверей" src={require('../img/slider_3.jpg')} />
                        </Carousel.Item>
                    </Carousel>
                </div>

                <div id="about" className="block">
                    <div className="item-1">
                        <h1 className="heading">О <span className="orange">«Моей</span> академии»:</h1>

                        <article className="article">
                            <p>«Моя академия» — это не просто самая большая в Омске сеть образовательных центров для детей и подростков, это центры детских достижений. Здесь ребёнок учится гармонично воспринимать себя и мир, расширяет кругозор, развивает все виды интеллекта и познавательные процессы: память, внимание, мышление, воображение.</p>
                            <p>Наша миссия — формирование счастливого будущего каждого ребёнка.</p>
                            <p>Мы используем образовательные комплексы, которые применяются в лучших образовательных центрах России и мира.  В основе этих программ — последовательное и непрерывное развитие личности, способной на любые достижения.</p>
                        </article>

                    </div>
                    <div className="item-2">
                        <h1 className="heading">Узнать <span className="orange">еще больше!</span></h1>
                        <InputForm action="api/registration/register" />
                    </div>
                </div>

                <div id="advantages" className="block">
                    <h1 className="heading item-1">Наши <span className="orange">преимущества:</span></h1>
                    <img className="item-2" src={require('../img/svg/jacket.svg')} />
                    <p className="item-3">Программы обучения и развития<br /> для детей и подростков<br /> (от 1 года до 17 лет)</p>
                    <img className="item-4" src={require('../img/svg/nametag.svg')} />
                    <p className="item-5">Мини-группы и<br /> индивидуальный подход</p>
                    <img className="item-6" src={require('../img/svg/apple.svg')} />
                    <p className="item-7">Здоровьесберегающие<br /> технологии — обучение в<br /> движении</p>
                    <img className="item-8" src={require('../img/svg/book.svg')} />
                    <p className="item-9">Лучшие методики<br /> и собственные разработки</p>
                    <img className="item-10" src={require('../img/svg/certificate.svg')} />
                    <p className="item-11">Только высококвалифицированные<br /> специалисты</p>
                    <img className="item-12" src={require('../img/svg/house.svg')} />
                    <p className="item-13">Большие и безопасные<br /> центры, близкие к Вам</p>
                </div>

                <div id="programms" className="block">
                    <h1 className="heading"><span className="orange">Образовательные и развивающие</span> программы:</h1>
                    <div className="program-block">

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_1">
                            <h1 className="card-text">АРТ-СТУДИЯ</h1>
                        </a>

                        <div className="modal fade" id="program_1" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">АРТ-СТУДИЯ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>В игровой форме дети отрабатывают все этапы создания художественного произведения — от наброска до последних штрихов. Педагоги помогают участникам курса развить художественные способности, творческое мышление и воображение.</p>
                                        <p> Занятия проходят 1-2 раза в неделю. Продолжительность — 50 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_2">
                            <h1 className="card-text">Я САМ!</h1>
                        </a>

                        <div className="modal fade" id="program_2" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">Я САМ!</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Программа готовит детей к посещению детского сада полного дня: адаптирует ребёнка к детскому коллективу, помогает понять роль педагога (воспитателя) и привыкнуть к распорядку дня, формирует умение выполнять инструкции.</p>
                                        <p>Направление включает в себя развивающие занятия и игры, полдник, свободную игровую деятельность, логоритмику.</p>
                                        <p>Занятия проходят 2-3 раза в неделю. Продолжительность — 120 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_3">
                            <h1 className="card-text">Я И МОЯ МАМОЧКА</h1>
                        </a>

                        <div className="modal fade" id="program_3" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">Я И МОЯ МАМОЧКА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Программа включает в себя комплекс разнообразных заданий: формирование математических представлений, развитие речи, психических процессов (мышления, внимания, памяти, восприятия и воображения), мелкой моторики, пальчиковую гимнастику, подготовку руки к письму, творческую деятельность.</p>
                                        <p>Дети проходят программу обязательно с близким взрослым человеком. Занятия проходят 2 раза в неделю. Продолжительность — 50 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_3a">
                            <h1 className="card-text">«ПРОГИМНАЗИЯ»: УТРЕННИЙ КОМПЛЕКС</h1>
                        </a>

                        <div className="modal fade" id="program_3a" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">«ПРОГИМНАЗИЯ»: УТРЕННИЙ КОМПЛЕКС</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>В состав программы входят:</p>
                                        <ul>
                                            <li>
                                                <p>развивающие занятия (подготовка к школе — чтение, развитие речи, логика, английский язык);</p>
                                            </li>
                                            <li>
                                                <p>занятия, направленные на развитие интеллекта, кругозора и эмоциональной сферы («Я и мир вокруг меня», курс интеллектуального развития);</p>
                                            </li>
                                            <li>
                                                <p>блок эстетического развития (арт-студия, логоритмика);</p>
                                            </li>
                                            <li>
                                                <p>блок физического развития (ритмика, детская йога).</p>
                                            </li>
                                        </ul>
                                        <p>Занятия проходят с понедельника по пятницу с 17:00 до 20:00.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_4">
                            <h1 className="card-text">«ПРОГИМНАЗИЯ»: ВЕЧЕРНИЙ КОМПЛЕКС</h1>
                        </a>

                        <div className="modal fade" id="program_4" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">«ПРОГИМНАЗИЯ»: ВЕЧЕРНИЙ КОМПЛЕКС</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>В состав программы входят:</p>
                                        <ul>
                                            <li>
                                                <p>развивающие занятия (подготовка к школе — чтение, математика, постановка руки и ориентация на листе бумаги, логика и развитие речи, развитие памяти, внимания, мышления);</p>
                                            </li>
                                            <li>
                                                <p>английский язык;</p>
                                            </li>
                                            <li>
                                                <p>арт-студия.</p>
                                            </li>
                                        </ul>
                                        <p>Занятия проходят с понедельника по пятницу с 17:00 до 20:00.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_5">
                            <h1 className="card-text">МИНИ-САД</h1>
                        </a>

                        <div className="modal fade" id="program_5" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">МИНИ-САД</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Дети проводят время в разновозрастной группе с одним воспитателем. В программу входят короткие развивающие занятия (20 минут), творческие уроки, чтение художественной литературы, свободная игровая деятельность.</p>
                                        <p>Мини-сад работает с понедельника по пятницу с 9:00 до 13:00.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_6">
                            <h1 className="card-text">ЛОГОРИТМИКА</h1>
                        </a>

                        <div className="modal fade" id="program_6" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ЛОГОРИТМИКА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Логоритмика включает в себя упражнения на развитие дыхание, голоса и артикуляции, задания, регулирующие мышечный тонус, активизирующие внимание, ритмические и общие физические упражнения, пение, танцетерпапию, пальчиковую гимнастику.</p>
                                        <p>Занятия проходят 1-2 раза в неделю. Продолжительность — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_7">
                            <h1 className="card-text">ИНОСТРАННЫЕ ЯЗЫКИ</h1>
                        </a>

                        <div className="modal fade" id="program_7" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ИНОСТРАННЫЕ ЯЗЫКИ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Мы учим детей второму «родному» языку. Обучение строится по уже знакомым и интуитивно понятным для ребенка принципам — слышу-повторяю-осознаю. Чтобы иностранный язык естественным образом усваивался, используются игровые элементы и мультимедийное обучение.</p>
                                        <p>Доступные программы: английский, немецкий, французский, испанский, китайский.</p>
                                        <p>Занятия проходят в группах (3-4 человека) и индивидуально.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_8">
                            <h1 className="card-text">КОМПЛЕКСНЫЕ ЗАНЯТИЯ</h1>
                        </a>

                        <div className="modal fade" id="program_8" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">КОМПЛЕКСНЫЕ ЗАНЯТИЯ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Программа включает в себя комплекс разнообразных заданий: формирование математических представлений, развитие речи, психических процессов (мышления, внимания, памяти, восприятия и воображения), мелкой моторики, пальчиковую гимнастику, подготовку руки к письму, творческую деятельность. С 4 лет дети знакомятся с буквами и учатся читать.</p>
                                        <p>Занятия проходят 2 раза в неделю. Продолжительность — 50 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_9">
                            <h1 className="card-text">Я И МИР ВОКРУГ МЕНЯ</h1>
                        </a>

                        <div className="modal fade" id="program_9" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">Я И МИР ВОКРУГ МЕНЯ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Как выглядит наша галактика? Почему дует ветер? Где живет вомбат и окапи? Какой голос у тасманского дьявола? В весёлой игровой форме дети получат ответы на эти и многие другие вопросы.</p>
                                        <p>Курс направлен на расширение кругозора и освоение основных лексических тем, необходимых ребенку для дальнейшего обучения в школе. Одна из частей занятия — знакомство с буквами, формирование и запоминание их образа.</p>
                                        <p>Продолжительность — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_10">
                            <h1 className="card-text">ПУТЕШЕСТВИЯ ВОКУРГ СВЕТА</h1>
                        </a>

                        <div className="modal fade" id="program_10" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ПУТЕШЕСТВИЯ ВОКУРГ СВЕТА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Программа позволит детям получить представление о странах и континентах, узнать об особенностях флоры и фауны каждой страны, познакомиться с достопримечательностями и традициями государств. Дети будут вести собственный дневник путешественника, создавать культурные объекты своими руками, играть в кукольном театре.</p>
                                        <p>Продолжительность занятия — 50 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_11">
                            <h1 className="card-text">ХОРЕОГРАФИЯ</h1>
                        </a>

                        <div className="modal fade" id="program_11" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ХОРЕОГРАФИЯ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Занятия эстрадно-спортивной хореографией сформируют у ребёнка красивую осанку, сильное и здоровое тело, научат чувствовать музыку, импровизировать, танцевать и выражать в танце свою индивидуальность.</p>
                                        <p>Занятия проходят 2 раза в неделю. Продолжительность — 60 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_12">
                            <h1 className="card-text">ШКОЛА ИНТЕЛЛЕКТА</h1>
                        </a>

                        <div className="modal fade" id="program_12" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ШКОЛА ИНТЕЛЛЕКТА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>В Школе интеллекта дети будут учиться размышлять, предполагать, выдвигать версии и получать удовольствие от нахождения решения. Занятия строятся в форме увлекательной игры, которая развивает у ребёнка не только логический и вербальный интеллект, но и социальный и эмоциональный. Дети учатся раскрепощённо общаться друг с другом, понимать и принимать свои эмоции и эмоции окружающих.</p>
                                        <p>Продолжительность — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_13">
                            <h1 className="card-text">ПОДГОТОВКА К ШКОЛЕ</h1>
                        </a>

                        <div className="modal fade" id="program_13" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ПОДГОТОВКА К ШКОЛЕ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>На курсе дети не только учатся читать, писать печатными буквами, считать, но и развивают внимание и память. Кроме того, формируются необходимые умственные действия дошкольника.</p>
                                        <p>Занятия проходят 2 раза в неделю. Продолжительность — 50 минут. Возможны индивидуальные уроки.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_14">
                            <h1 className="card-text">ДЕТСКАЯ ЙОГА</h1>
                        </a>

                        <div className="modal fade" id="program_14" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ДЕТСКАЯ ЙОГА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Детская йога — специальные комплексы упражнений, которые проводятся, как правило, в форме игры «в животных». Основная задача программы — развить в ребёнке осознанность, внимательное и уважительное отношение к своему телу, к своему состоянию и настроению, укрепить его здоровье и поддержать самооценку.</p>
                                        <p>Занятия проходят 2 раза в неделю. Продолжительность – 45 минут. Возможно проведение индивидуальных занятий.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_15">
                            <h1 className="card-text">ЛОГИКА</h1>
                        </a>

                        <div className="modal fade" id="program_15" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ЛОГИКА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>В процессе школьного обучения устойчивый успех приходит к тому, кто делает точные выводы, действует разумно и мыслит последовательно. Курс «Логика» развивает мыслительные способности, внимание, память, речь, мелкую моторику.</p>
                                        <p>Занятия проходят 1-2 раза в неделю. Продолжительность — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_16">
                            <h1 className="card-text">ЧТЕНИЕ</h1>
                        </a>

                        <div className="modal fade" id="program_16" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ЧТЕНИЕ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Программа формирует навыки правильного чтения и готовит к грамотному письму. Особое внимание уделяется развитию фонематического слуха, а также памяти, внимания и восприятия, которые оказывают решающее влияние на скорость чтения.</p>
                                        <p>Формы обучения — групповые и индивидуальные занятия. Групповые занятия проходят 2 раза в неделю. Длительность занятия — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_17">
                            <h1 className="card-text">ТЕАТР</h1>
                        </a>

                        <div className="modal fade" id="program_17" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ТЕАТР</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Студия поможет детям развить артистические способности, научит свободно общаться и уверенно чувствовать себя в любых ситуациях, правильно доносить до окружающих свои мысли, эмоции и чувства.</p>
                                        <p>Программа включают в себя занятия сценической речью и сценическому движению, уроки актёрского мастерства и движения под музыку.</p>
                                        <p>Занятия проходят 2 раза в неделю по 1,5 часа.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_18">
                            <h1 className="card-text">МЕНТАЛЬНАЯ АРИФМЕТИКА</h1>
                        </a>

                        <div className="modal fade" id="program_18" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">МЕНТАЛЬНАЯ АРИФМЕТИКА</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Ментальная арифметика не только помогает освоить навыки быстрого вычисления, но и способствует развитию аналитических способностей. Задача курса — на занятиях задействовать весь мозг с помощью выполнения операций на счётах обеими руками. Таким образом, включается в работу и правое полушарие, которое отвечает за творчество, и левое, которое отвечает за логику.</p>
                                        <p>Занятия проходят 2 раза в неделю. Продолжительность занятия — 60 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_19">
                            <h1 className="card-text">ШКОЛА ЭКСПЕРИМЕНТОВ</h1>
                        </a>

                        <div className="modal fade" id="program_19" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ШКОЛА ЭКСПЕРИМЕНТОВ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>«Школа экспериментов» построена так, что каждый ребёнок становится полноправным участником научных опытов. Участники курса познают законы окружающего мира, находят объяснения сложным явлениям природы, учатся наблюдать, предполагать, сопоставлять и делать выводы.</p>
                                        <p>Занятия проходят 1 раз в неделю. Продолжительность — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_20">
                            <h1 className="card-text">РАЗВИТИЕ РЕЧИ</h1>
                        </a>

                        <div className="modal fade" id="program_20" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">РАЗВИТИЕ РЕЧИ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Курс по развитию речи стимулирует все стороны развития ребёнка — коммуникативные навыки, познавательную активность, внимание, память, мышление. На занятиях формируется правильное дыхание, развивается артикуляция, совершенствуется связная речь, обогащается словарный запас.</p>
                                        <p>Формы обучения — групповые и индивидуальные. Занятия в группе проходят 1- 2 раза в неделю. Длительность занятия — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_21">
                            <h1 className="card-text">ЭКСПРЕСС ПОДГОТОВКА К ШКОЛЕ</h1>
                        </a>

                        <div className="modal fade" id="program_21" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ЭКСПРЕСС ПОДГОТОВКА К ШКОЛЕ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Курс формирует основные навыки подготовки к школе — умение читать, писать и считать. Кроме того, дети учатся работать в тетради, использовать различные инструменты в творческой деятельности (ножницы, краски, пластилин и т.д.), приобретают необходимый объём знаний об окружающем мире.</p>
                                        <p>Формы обучения — групповые и индивидуальные занятия. Групповые занятия - 2 раза в неделю. Длительность занятия — 50 + 50 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_22">
                            <h1 className="card-text">ШКОЛА ВНИМАНИЯ</h1>
                        </a>

                        <div className="modal fade" id="program_22" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ШКОЛА ВНИМАНИЯ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Именно в начальной школе важно научить ребёнка не отвлекаться, сосредотачиваться и концентрироваться на выполнении определённой задачи. Занятия включают целенаправленную двигательную активность и работу за столом при выполнении упражнений. Все задания направлены не только на развитие внимания, но и восприятия, памяти, мышления.</p>
                                        <p>Занятия проходят 1 раз в неделю. Продолжительность — 45 минут.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_23">
                            <h1 className="card-text">ПОДГОТОВКА К ОГЭ и ЕГЭ</h1>
                        </a>

                        <div className="modal fade" id="program_23" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ПОДГОТОВКА К ОГЭ и ЕГЭ</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Курс составлен экспертом комиссии государственной итоговой аттестации выпускников по иностранному языку. Программа направлена на комплексную подготовку к экзамену по английскому. Подготовка ведется по следующим направлениям: наращивание словарного запаса, развитие всех речевых умений, работа с типичными ошибками.</p>
                                        <p>Занятия проходят 2 раза в неделю по 2 академических часа. Продолжительность курса — 8 месяцев.</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#programms" className="thumb program" style={randomColor()} data-toggle="modal" data-target="#program_24">
                            <h1 className="card-text">ЛОГОПЕД</h1>
                        </a>

                        <div className="modal fade" id="program_24" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="program_1Title">ЛОГОПЕД</h5>
                                    </div>
                                    <div className="modal-body">
                                        <p>Логопед поможет ребёнку овладеть красивой речью и правильным звукопроизношением. Работа начинается с проведения диагностики и последующей беседы с родителями. Занятия со специалистом проходят индивидуально.</p>                                        
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">Вернуться</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="starteducation" className="block">
                    <div className="item-1">
                        <h1 className="heading"><span className="orange">Запишитесь</span> на пробное занятие:</h1>
                        <InputForm action="api/registration" />
                    </div>
                    <div className="item-2">
                        <img className="trial-image" src={require('../img/trial.png')} />
                    </div>
                </div>

                <div id="contacts" className="block">
                    <div className="item-1">
                        <iframe className="map" src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1924.624840282746!2d73.35624561133196!3d54.994534025278035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z0J7QvNGB0LogQUJDIGNsdWI!5e0!3m2!1sru!2sru!4v1533660924368"></iframe>
                    </div>
                    <div className="item-2">
                        <h1 className="heading"><span className="orange">Целых 5 центров!</span></h1>
                        <h3 className="sub-heading">Обязательно найдется удобный именно для Вас.</h3>
                        <ul className="centers">
                            <li className="center">ул. 70 лет Октября, 20</li>
                            <li className="center">ул. Циолковского, 6/1</li>
                            <li className="center">ул. Бульвар Зелёный, 10/2</li>
                            <li className="center">ул. Декабристов, 104</li>
                            <li className="center">ул. Волочаевская, 11/1</li>
                        </ul>
                        <h3 className="sub-heading phone">+7 (3812) 90-01-02</h3>
                    </div>
                </div>

                <div id="partners" className="block">
                    <h1 className="heading">Наши <span className="orange">друзья:</span></h1>
                    <div id="abc-club" className="partner">
                        <a className="partner-link" target="_blank" rel="noopener noreferrer" href="http://www.abcomsk.ru/">
                            <img className="partner-logo" src={require('../img/svg/abcclub_logo.svg')} />
                        </a>
                    </div>
                    <div id="bembi" className="partner">
                        <img className="partner-logo" src={require('../img/bembi_logo.png')} />
                    </div>
                </div>

            </main>

            <footer className="footer">
                <p id="copyright">Моя академия. Все права защищены.</p>
                <p id="year">2018.</p>
                <p id="wish">Будем рады подружиться!</p>

                <div className="socials">
                    <a className="social-item" target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/akademy_center/"><i className="fab fa-instagram fa-3x" /></a>
                    <a className="social-item" target="_blank" rel="noopener noreferrer" href="https://vk.com/academy_center"><i className="fab fa-vk fa-3x" /></a>
                    <a className="social-item" target="_blank" rel="noopener noreferrer" href="https://ok.ru/profile/561815399930"><i className="fab fa-odnoklassniki fa-3x" /></a>
                </div>

            </footer>

            <YMInitializer accounts={[50113915]} />
        </div >
    }
}
